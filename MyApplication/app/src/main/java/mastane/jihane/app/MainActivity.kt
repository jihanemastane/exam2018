package mastane.jihane.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gitrepo2.myAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.detailscontact_layout.*
import kotlinx.android.synthetic.main.eleve_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(), OnListClickListener {
    val newliste = ArrayList<modelContact>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val builder = Retrofit.Builder()
            .baseUrl("https://api.myjson.com/")
            .addConverterFactory(GsonConverterFactory.create())


        val retrofit = builder.build()
        val myInterface = retrofit.create(serviceInterface::class.java)
        val call = myInterface.getContact("bins")

        call.enqueue(object : Callback<List<modelContact>>{

            override fun onResponse(call: Call<List<modelContact>>, response: Response<List<modelContact>>) {
                val results = response.body()


                newliste.addAll(results!!)
                myRecyclerView.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL ,false)
                myRecyclerView.adapter = myAdapter(newliste,this@MainActivity)
            }

            override fun onFailure(call: Call<List<modelContact>>, t: Throwable) {

                Toast.makeText(this@MainActivity, "Error...!!!", Toast.LENGTH_SHORT).show();
            }


        })
    }

    override fun onListClick(position: Int) {
        val intent = Intent(this,Details::class.java)
        intent.putExtra("nom", newliste[position].name.last + " " + newliste[position].name.first)
        intent.putExtra("address", newliste[position].address.city +" " + newliste[position].address.state +" " + newliste[position].address.street +" " + newliste[position].address.zip)
        intent.putExtra("picture", newliste[position].picture)
        intent.putExtra("linkedin", newliste[position].linkedin)
        intent.putExtra("tel", newliste[position].phone)


        startActivity(intent)
    }
}