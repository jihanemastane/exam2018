package mastane.jihane.app

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detailscontact_layout.*
import java.net.URI


class Details:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detailscontact_layout);

       val queue = Volley.newRequestQueue(this@Details)
        val tel = intent.getSerializableExtra("tel")
        val address = intent.getSerializableExtra("address")
        val picture = intent.getSerializableExtra("picture")
        val linkedin = intent.getSerializableExtra("linkedin")
        val name = intent.getSerializableExtra("nom")


        Picasso.get()
            .load(picture.toString())
            .placeholder(R.mipmap.ic_launcher_round)
            .into(detailspic);
        detailNom.text = name.toString()
        detailsAdresse.text = address.toString()
        detailsTel.text = tel.toString()

        detailsLinkedin.setOnClickListener{
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(linkedin.toString()))
            startActivity(intent)
        }

        detailsMap.setOnClickListener {
            val intent : Intent = packageManager.getLaunchIntentForPackage("com.google.android.apps.maps")!!
            intent.addCategory(Intent.CATEGORY_LAUNCHER)

            startActivity(intent)
        }

        detailsAppeler.setOnClickListener {
            val tell = tel.toString()
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel"+ tell))
            startActivity(intent)
        }


    }





}