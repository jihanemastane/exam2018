package mastane.jihane.app
import com.google.gson.annotations.SerializedName

data class modelContact(

    @SerializedName("address")
    val address: Address,

    @SerializedName("email")
    val email: String,

    @SerializedName("linkedin")
    val linkedin: String,

    @SerializedName("name")
    val name: Name,

    @SerializedName("nationalities")
    val nationalities: List<Nationality>,

    @SerializedName("phone")
    val phone: String,

    @SerializedName("picture")
    val picture: String
)

data class Address(
    @SerializedName("city")
    val city: String,

    @SerializedName("state")
    val state: String,

    @SerializedName("street")
    val street: String,

    @SerializedName("zip")
    val zip: Int
)
data class Name(
    @SerializedName("first")
    val first: String,

    @SerializedName("last")
    val last: String
)
data class Nationality(
    @SerializedName("flag")
    val flag: String,

    @SerializedName("nationality")
    val nationality: String
)