package com.example.gitrepo2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mastane.jihane.app.OnListClickListener
import mastane.jihane.app.R
import mastane.jihane.app.modelContact

class myAdapter (val modelContact: ArrayList<modelContact>, private val onListClickListener: OnListClickListener): RecyclerView.Adapter<myAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.eleve_layout, parent, false)
        return ViewHolder(v, onListClickListener)
    }

    override fun getItemCount()=modelContact.size

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {


        val item: modelContact = modelContact[position]
        holder.nom.text = item.name.first + " " + item.name.last;
        holder.email.text = item.email
        holder.tel.text = item.phone
        Picasso.get()
            .load(item.picture)
            .placeholder(R.mipmap.ic_launcher_round)
            .into(holder.pic)


    }
    class ViewHolder(itemView: View, private val onListClickListener: OnListClickListener ) : RecyclerView.ViewHolder(itemView) {
        val nom : TextView = itemView.findViewById(R.id.nom)
        val email : TextView = itemView.findViewById(R.id.email)
        val tel : TextView = itemView.findViewById(R.id.tel)
        val pic = itemView.findViewById<ImageView>(R.id.pic)

        init {
            itemView.setOnClickListener {
                onListClickListener.onListClick(adapterPosition)
            }
        }


    }


}
